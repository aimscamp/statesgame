package com.example.lynn.statesgame;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.lynn.statesgame.MainActivity.*;

/**
 * Created by lynn on 6/26/2015.
 */
public class MyImageView extends RelativeLayout {
    private int id;
    private ImageView imageView;
    private TextView textView;

    public MyImageView(Context context) {
        super(context);

        imageView = new ImageView(context);

        imageView.setId(R.id.imageview);

        textView = new TextView(context);

        textView.setId(R.id.textview);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(500,500);

        setLayoutParams(layoutParams);

     //   RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

     //   layoutParams.addRule(RelativeLayout.BELOW,R.id.imageview);

     //   addView(imageView);

     //   textView.setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(500,450);

        layoutParams.leftMargin = 0;

        imageView.setLayoutParams(layoutParams);

        addView(imageView);

        layoutParams = new RelativeLayout.LayoutParams(500,50);

        layoutParams.topMargin = 450;

        textView.setLayoutParams(layoutParams);

        addView(textView);

    }

    public int getId() {
        return(id);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void loadDrawable() {
        imageView.setImageDrawable(getResources().getDrawable(id));

        textView.setText(reverseMap.get(id));
    }

    public void removeDrawable() {
        imageView.setImageDrawable(null);

        textView.setText("");
    }
}
