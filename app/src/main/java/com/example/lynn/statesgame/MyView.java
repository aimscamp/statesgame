package com.example.lynn.statesgame;

import android.content.Context;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import static com.example.lynn.statesgame.MainActivity.*;

/**
 * Created by lynn on 6/25/2015.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        String[] names = {"Alabama","Alaska","Arizona","Arkansas","California","Colorado",
                "Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho",
                "Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana",
                "Maine","Maryland","Massachusetts","Michigan","Minnesota",
                "Mississippi","Missouri","Montana","Nebraska","Nevada",
                "New Hampshire","New Jersey","New Mexico","New York",
                "North Carolina","North Dakota","Ohio","Oklahoma","Oregon",
                "Pennsylvania","Rhode Island","South Carolina","South Dakota",
                "Tennessee","Texas","Utah","Vermont","Virginia","Washington",
                "West Virginia","Wisconsin","Wyoming"};

        imageIds = State.getImageIds(this);

        reverseMap = State.getReverseMap(this);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        playGame.setText("Play Game");

        playGame.setLayoutParams(layoutParams);

        playGame.setOnClickListener(listener);

        addView(playGame);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,700);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.playgame);

        statesView.setLayoutParams(layoutParams);

        addView(statesView);

        for (int counter=0;counter<states.length;counter++)
            states[counter].setOnTouchListener(listener);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.statesview);

        submitAnswer.setText("Submit Answer");

        submitAnswer.setLayoutParams(layoutParams);

        submitAnswer.setOnClickListener(listener);

        addView(submitAnswer);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.submitanswer);
        layoutParams.addRule(RelativeLayout.BELOW,R.id.statesview);

        message = new TextView(context);

        message.setLayoutParams(layoutParams);

        addView(message);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.submitanswer);

        answers[0].setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.answer1);

        answers[1].setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.answer2);

        answers[2].setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.answer3);

        answers[3].setLayoutParams(layoutParams);

        for (int counter=0;counter<answers.length;counter++)
            addView(answers[counter]);
    }

}
