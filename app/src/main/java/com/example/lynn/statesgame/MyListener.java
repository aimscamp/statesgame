package com.example.lynn.statesgame;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.example.lynn.statesgame.MainActivity.*;

/**
 * Created by lynn on 6/25/2015.
 */
public class
        MyListener implements View.OnClickListener, View.OnTouchListener {
    private int offsetX;
    private int offsetY;

    @Override
    public void onClick(View v) {
        final Map<String,Double> areas = State.getAreas();

        if (v == playGame) {
            List<String> list = new ArrayList<>();

            list.addAll(areas.keySet());

            int[] indices = new int[4];

            indices[0] = (int)(list.size()*Math.random());

            Set<Integer> usedIndices = new HashSet<>();

            usedIndices.add(indices[0]);

            for (int counter=1;counter<indices.length;counter++) {
                int temp = (int)(list.size()*Math.random());

                while (usedIndices.contains(temp))
                    temp = (int)(list.size()*Math.random());

                indices[counter] = temp;

                usedIndices.add(temp);
            }

            for (int counter=0;counter<states.length;counter++) {
                states[counter].setId(imageIds.get(list.get(indices[counter])));

                states[counter].loadDrawable();

                answers[counter].setText("");
            }

            message.setText("");
        } else if (v == submitAnswer) {
            Toast.makeText(v.getContext(), "Hello", Toast.LENGTH_LONG).show();

            TreeSet<MyImageView> temp = new TreeSet<>(new Comparator<MyImageView>() {

                @Override
                public int compare(MyImageView view1, MyImageView view2) {
                    RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams)view1.getLayoutParams();
                    RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams)view2.getLayoutParams();

                    if (layoutParams1.leftMargin < layoutParams2.leftMargin)
                        return(-1);
                    else if (layoutParams1.leftMargin > layoutParams2.leftMargin)
                        return(1);
                    else
                        return 0;
                }

            });

            for (int counter=0;counter<states.length;counter++)
                temp.add(states[counter]);

            MyImageView[] imageViewsOrderedByPosition = temp.toArray(new MyImageView[temp.size()]);

            temp = new TreeSet<>(new Comparator<MyImageView>() {

                @Override
                public int compare(MyImageView view1, MyImageView view2) {
                    if (areas.get(reverseMap.get(view1.getId())) > areas.get(reverseMap.get(view2.getId())))
                        return(-1);
                    else if (areas.get(reverseMap.get(view1.getId())) < areas.get(reverseMap.get(view2.getId())))
                        return(1);
                    else
                        return 0;
                }

            });

            for (int counter=0;counter<states.length;counter++)
                temp.add(states[counter]);

            MyImageView[] imageViewsInCorrectOrder = temp.toArray(new MyImageView[temp.size()]);

            if (Arrays.equals(imageViewsOrderedByPosition, imageViewsInCorrectOrder))
                message.setText("Correct");
            else {
                message.setText("Incorrect");

                for (int counter=0;counter<imageViewsInCorrectOrder.length;counter++) {
                    String state = reverseMap.get(imageViewsInCorrectOrder[counter].getId());

                    double area = areas.get(state);

                    answers[counter].setText(state.substring(0, 1).toUpperCase() + state.substring(1).toLowerCase() + " has " + area + " square miles");
                }
            }
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)v.getLayoutParams();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            offsetX = (int)(event.getRawX() - layoutParams.leftMargin);
            offsetY = (int)(event.getRawY() - layoutParams.topMargin);
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            layoutParams.leftMargin = (int)(event.getRawX() - offsetX);
            layoutParams.topMargin = (int)(event.getRawY() - offsetY);

            v.setLayoutParams(layoutParams);
        }

        return(true);
    }
}
